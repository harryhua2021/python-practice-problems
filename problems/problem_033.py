# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def sum_of_first_n_even_numbers(n):
    if n < 0:                                           # solution
        return None                                     # solution
    sum = 0                                             # solution
    for i in range(n + 1):                              # solution
        sum = sum + i * 2                               # solution
    return sum                                          # solution


print(sum_of_first_n_even_numbers(1))
print(sum_of_first_n_even_numbers(2))
print(sum_of_first_n_even_numbers(5))


# def sum_of_first_n_even_numbers(n):
#     if n < 0:
#         return None

#     even_sum = 0
#     for num in range(n):
#         if num % 2 == 0:
#             even_sum += num

#     return num

# print(sum_of_first_n_even_numbers(1))
# print(sum_of_first_n_even_numbers(2))
# print(sum_of_first_n_even_numbers(5))



# print(sum_of_first_n_even_numbers(1))


# def sum_of_first_n_even_numbers(n):
#     if n < 0:
#         return None

#     even_sum = 0
#     for i in range(n):
#         even_number = 2 * i
#         even_sum += even_number

#     return even_sum

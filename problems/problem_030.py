# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if not values:
        return ''
    if len(values) == 1:
        return ''

    unique_values = set(values)
    unique_values.remove(max(unique_values))

    return max(unique_values)


value_list = [3, 5, 2, 8, 9]
print(find_second_largest(value_list))

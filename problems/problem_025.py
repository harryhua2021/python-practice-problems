# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    for num in values:
        return sum(num)


num = [1, 2, 3, 4, 5]
print(sum(num))

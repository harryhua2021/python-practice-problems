# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def is_palindrome(word):
    # loop over each letter in the value of word.
#     palindrome = ["r", "a", "c", "e", "c", "a", "r"]
#     for letter in word:
#         if letter in palindrome == word.reverse():
#          return is_palindrome
#     else:
#         return False

# def isPalindrome(s):
#     return s == s[::-1]

# def is_palindrome(word):
#     return word == word[::-1]
# # Driver code
# word = "racecar"
# reverse_word = is_palindrome(word)

# if reverse_word:
#     print("Yes")
# else:
#     print("No")

def is_Palindrome(word):
    reversed_word = "".join(list(reversed(word)))
    if word == reversed_word:
        return True
    return False

print(is_Palindrome("racecar"))
print(is_Palindrome("notracecar"))

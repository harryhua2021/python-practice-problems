# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}


def reverse_dictionary(dictionary):
    new_dictionary = {}                                 # solution
    for key, value in dictionary.items():               # solution
        new_dictionary[value] = key                     # solution
    return new_dictionary

dog = {
    "name": "Buddy",
    "breed": "Husky",
    "city": "Tucson",
}

print(reverse_dictionary(dog))







# dog = {
#     "name": "Buddy",
#     "breed": "Husky",
#     "city": ["Tucson", "Seattle","New York"]
# }

# # dog_name = dog.get("city")
# # print(dog_name)

# print(dog[2],["city"])
# # print(my_combined_list[2],.get("city"))

# dog = {
#     "name": "Buddy",
#     "breed": "Husky",
#     "city": "Tucson",
# }

# deleted_value = dog.pop("name")
# del dog["city"]

# print(dog)
# print(dog)
# print(deleted_value)

# dog = {
#     "name": "Buddy",
#     "breed": "Husky",
#     "city": "Tucson",
# }

# for item in dog:
#     print(item)

# for k,v in dog.items():
#     print(k + " " + v)

# print(list(dog.values()))

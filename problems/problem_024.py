# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
        if len(values) == 0:
            return None
        average = sum(values)/len(values)
        return average


numbers = [1, 2, 3, 4, 5]
average = calculate_average(numbers)
print(average)



# def calculate_average(values):
#     if len(values) == 0:
#         return None

#     total = sum(values)
#     avg = total / len(values)
#     return avg

# # Example usage
# numbers = [1, 2, 3, 4, 5]
# average = calculate_average(numbers)
# print("The average is:", average)
